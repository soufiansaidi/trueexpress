import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'

import Home from './components/pages/home.vue'
import Cart from './components/pages/cart.vue'
import Category from './components/pages/category.vue'
import Checkout from './components/pages/checkout.vue'
import Login from './components/pages/login.vue'
import Product from './components/pages/product.vue'
import Register from './components/pages/register.vue'

Vue.use(VueRouter)

const routes = [
  {path: '/', component: Home},
  {path: '/cart', component: Cart},
  {path: '/category', component: Category},
  {path: '/checkout', component: Checkout},
  {path: '/login', component: Login},
  {path: '/product', component: Product},
  {path: '/register', component: Register}
]

const router = new VueRouter({
  routes,
  mode: 'history'
})

import './assets/css/style.css'

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
