$(".toggle-menu i").click(function(){

    $(".mobile-menu").addClass('active');

});

$(".closeMenu").click(function(){

    $(".mobile-menu").removeClass('active');

});



var list = $(".mobile-menu ul li a");

list.click(function (event) {

  var submenu = this.parentNode.getElementsByTagName("ul").item(0);

  if(submenu!=null){

    event.preventDefault();

    $(submenu).slideToggle('fast');

    $(submenu).parent().toggleClass('active');

  }

});

$(".input-quantity input[type='button']").click(function(){
  $input = $(this).closest('.input-quantity').find('input[name="quantity"]');
  $div = $(this).closest('.input-quantity').find('#value-quality');
  if($(this).attr('id') == "plus"){
    $count = parseInt($input.val()) + 1;
    $input.val($count.toString());
    $div.text($count.toString());
  }else{
    $count = parseInt($input.val());
    if($count > 1){
      $count--;
      $input.val($count.toString());
      $div.text($count.toString());
    }
  }
});



$(".password-input i").click(function(){

  $(this).parent().toggleClass("active");

  $(this).toggleClass("fa-eye-slash");

  if($(this).closest('.password-input').find('input').attr("type") == "password"){

    $(this).closest('.password-input').find('input').attr("type", "text");

  }else{

    $(this).closest('.password-input').find('input').attr("type", "password");

  }

});



$(".nicemenu").nicemenu();



$(window).on("load", function(){$(".loading").fadeOut();});



$(".title-filter").click(function () {

  $(this).toggleClass("active");

  $(this).closest(".filter-part").find(".filter-content").slideToggle('fast');

});



$('.Monthly').owlCarousel({

    loop:false,

    items:6,

    margin:15,

    nav:true,

    navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],

    dots:false,

    responsive:{

      0:{

        items:1

      },

      360:{

        items:2

      },

      768:{

        items:3

      },

      991:{

        items:4

      },

      1100:{

        items:5

      },

      1200:{

        items:6

      }

    }

});

$('.category').owlCarousel({

    loop:false,

    items:6,

    margin:15,

    nav:true,

    navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],

    dots:false,

    responsive:{

      0:{

        items:1

      },

      360:{

        items:2

      },

      768:{

        items:3

      },

      991:{

        items:4

      },

      1100:{

        items:5

      },

      1200:{

        items:6

      }

    }

});

$('.parfume').owlCarousel({

    loop:false,

    items:6,

    margin:15,

    nav:true,

    navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],

    dots:false,

    responsive:{

      0:{

        items:1

      },

      360:{

        items:2

      },

      768:{

        items:3

      },

      991:{

        items:4

      },

      1100:{

        items:5

      },

      1200:{

        items:6

      }

    }

});

$('.laptop').owlCarousel({

    loop:false,

    items:6,

    margin:15,

    nav:true,

    navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],

    dots:false,

    responsive:{

      0:{

        items:1

      },

      360:{

        items:2

      },

      768:{

        items:3

      },

      991:{

        items:4

      },

      1100:{

        items:5

      },

      1200:{

        items:6

      }

    }

});

$('.tshirt').owlCarousel({

    loop:false,

    items:6,

    margin:15,

    nav:true,

    navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],

    dots:false,

    responsive:{

      0:{

        items:1

      },

      360:{

        items:2

      },

      768:{

        items:3

      },

      991:{

        items:4

      },

      1100:{

        items:5

      },

      1200:{

        items:6

      }

    }

});

$('.descount').owlCarousel({

    loop:false,

    items:6,

    margin:15,

    nav:true,

    navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],

    dots:false,

    responsive:{

      0:{

        items:1

      },

      360:{

        items:2

      },

      768:{

        items:3

      },

      991:{

        items:4

      },

      1100:{

        items:5

      },

      1200:{

        items:6

      }

    }

});

